class CreateInquiryMails < ActiveRecord::Migration
  def up
    create_table :inquiry_mails do |t|
      t.string "email", :null => false, :limit => 150
      t.string "subject", :null => false, :limit => 200
      t.text "content", :null => false
      t.timestamps
    end
  end

  def down
  	drop_table :inquiry_mails
  end
end
