include ERB::Util

class SitesController < ApplicationController
	def home
		params[:page] = "home"
	end

	def about
		params[:page] = "about"
	end

	def product
		params[:page] = "product"
	end

	def contact
		params[:page] = "contact"
		@inquiry = InquiryMail.new
	end

	def sendMail
		if params[:inquiry].present?
			params[:page] = "contact"    
			@inquiry = InquiryMail.new(inquiryMail_param)
			if (@inquiry.save) 
				InquiryMailer.sendInquiry(@inquiry).deliver
				error_str = "Inquiry submitted!<br>We will get back to you as soon as possible<br>Thank you!".html_safe
				flash.now[:alert] = error_str
			end
			render("contact")
		else
			redirect_to(:action => "contact")
		end
	end

 	private

	    def inquiryMail_param
			params[:subject] = ActionController::Base.helpers.sanitize(params[:subject])
			params[:content] = ActionController::Base.helpers.sanitize(params[:content])
			params.require(:inquiry).permit(:email, :subject, :content)
	    end
end
